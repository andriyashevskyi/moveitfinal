import React from 'react';
import PropTypes from 'prop-types';

function Tip({className, text}) {
  return (
      <div className={className}>{text}</div>
  )
}

Tip.propTypes = {
  className: PropTypes.string,
  text: PropTypes.string.isRequired
};
Tip.defaultProps = {
  className: 'tip-block',
  text: "Here tip for you",
};

export default Tip;