import React from "react";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";
import {
  readyToProceed,
  unreadyToProceed,
  setDataToProceed
} from "../actions/orderProceed";

import Paper from "@material-ui/core/Paper";
import Radio from "@material-ui/core/Radio";

import {
  required,
  phoneNumber,
  email,
  renderField,
  lastName,
  firstName,
  address,
  area,
  bigThings
} from "../formValidators";

import { getAddressesId } from "../formValidators/addOrderSubmitValidator";

import { submit } from "../formValidators/addOrderSubmitValidator";

import { startAddOrder } from "../actions/orders";

import StyledInput from "../views/StyledInput";
// import RadioGroup from "@material-ui/core/RadioGroup";

import { RadioButtonGroup } from "redux-form-material-ui";
import { TextField, RadioGroup } from "redux-form-material-ui";

let AddOrderForm = props => {
  const {
    handleSubmit,
    submitting,
    pristine,
    initialValues,
    unreadyToProceed,
    orderReadyToProceed,
    startAddOrder,
    price,
    distance
  } = props;
  const al = true;
  const valid = [required, address];
  return (
    <form
      onSubmit={
        !orderReadyToProceed
          ? handleSubmit(submit)
          : e => {
              e.preventDefault();
              startAddOrder();
            }
      }
    >
      <div
        style={{
          display: "grid",
          gridTemplateColumns: "80%",
          gridColumnGap: "20px",
          gridRowGap: "20px",
          justifyContent: "center",
          margin: "0 auto",
          padding: "20px"
        }}
      >
        <Paper style={{ padding: "16px" }}>
          <Field
            name="firstName"
            component={TextField}
            validate={[required, firstName]}
            type="text"
            label={"Имя"}
            required
            placeholder="Введите свое имя"
            fullWidth
            margin="dense"
          />

          <Field
            name="lastName"
            component={TextField}
            validate={[required, lastName]}
            type="text"
            placeholder="Введите свою фамилию"
            label={"Фамилия"}
            required
            fullWidth
            margin="dense"
          />

          <Field
            name="phone"
            type="tel"
            component={TextField}
            label="Номер телефона"
            placeholder="Введите номер мобильного"
            validate={[required, phoneNumber]}
            required
            fullWidth
            margin="dense"
          />

          <Field
            name="email"
            component={TextField}
            type="text"
            placeholder="Email"
            label="Email"
            validate={[required, email]}
            required
            disabled={!!orderReadyToProceed}
            fullWidth
            margin="dense"
          />
        </Paper>

        <Paper
          style={{
            padding: "16px"
          }}
        >
          <div
            style={{
              display: "grid",
              gridTemplateColumns: "1fr 1fr",
              gridColumnGap: "20px"
            }}
          >
            <Field
              name="originalAddress"
              type="text"
              placeholder="Адмирала Макарова 58, Николаев"
              label="Откуда"
              component={TextField}
              required
              validate={valid}
              disabled={!!orderReadyToProceed}
              fullWidth
              margin="dense"
            />

            <Field
              name="destinationAddress"
              type="text"
              placeholder="Милютенко 45, Киев"
              label="Куда"
              component={TextField}
              validate={[required, address]}
              disabled={!!orderReadyToProceed}
              fullWidth
              margin="dense"
            />
          </div>
        </Paper>

        <Paper style={{ padding: "16px" }}>
          <Field
            name="liveArea"
            type="number"
            placeholder="Введите площадь жилой площади в кв.м. Например: 15"
            label="Жилая Площадь"
            component={TextField}
            validate={[required, area]}
            disabled={!!orderReadyToProceed}
            fullWidth
            margin="dense"
          />

          <Field
            name="backArea"
            type="number"
            placeholder="Введите площадь нежилой площади в кв.м. Например: 2"
            label="Нежилая площадь"
            component={TextField}
            validate={[required, area]}
            disabled={!!orderReadyToProceed}
            fullWidth
            required
            margin="dense"
          />

          <Field
            name="bigThings"
            type="text"
            placeholder="Например: пианино, рояль и т.д. Если таких вещей нету, то поставьте -  ."
            label="Крупногабаритные предметы"
            required
            component={TextField}
            validate={[required, bigThings]}
            disabled={!!orderReadyToProceed}
            margin="dense"
          />
          {/* <RadioButtonGroup>
<Radio value="yes" label="Да" />
            <Radio value="no" label="Нет" />
          </RadioButtonGroup>
          <Field
            name="packing"
            component={RadioButtonGroup}
            label="Нужна ли вам помощь с упаковкой предметов?"
          >
            <Radio value="yes" label="Да" />
            <Radio value="no" label="Нет" />
          </Field> */}
          <Field name="bestFramework" component={RadioButtonGroup}>
            <Radio value="react" label="React" />
            <Radio value="angular" label="Angular" />
            <Radio value="ember" label="Ember" />
          </Field>

          {/* <div>
            <label>Нужна ли вам помощь с упаковкой предметов?</label>
            <div>
              <label>
                <Field
                  name="packing"
                  component="input"
                  type="radio"
                  value="yes"
                  disabled={orderReadyToProceed ? true : null}
                />{" "}
                Yes
              </label>
              <label>
                <Field
                  name="packing"
                  component="input"
                  type="radio"
                  value="no"
                  disabled={orderReadyToProceed ? true : null}
                />{" "}
                NO
              </label>
            </div>
          </div> */}
        </Paper>

        {!orderReadyToProceed ? (
          <div>
            <button
              type="submit"
              disabled={pristine || submitting}
              className="styled-btn"
            >
              расчитать цену
            </button>
          </div>
        ) : (
          <div className="information-block">
            <div className="information-block__header">
              <h2>Your price</h2>
            </div>
            <div className="information-block__content">
              <span>price: {price}</span>
              <span>
                distance:
                {distance}
              </span>
              <button onClick={unreadyToProceed} className="styled-btn">
                Назад
              </button>
              <button type="submit" className="styled-btn">
                Оформить перевозку
              </button>
            </div>
          </div>
        )}
      </div>
    </form>
  );
};

const mapStateToProps = state => {
  if (state.userSettings.useData) {
    return {
      initialValues: { ...state.userSettings, ...{ packing: "no" } },
      orderReadyToProceed: state.proceedOrder.orderReadyToProceed,
      price: state.proceedOrder.price,
      distance: state.proceedOrder.distance
    };
  } else {
    return {
      initialValues: { ...{ packing: "no" } },
      orderReadyToProceed: state.proceedOrder.orderReadyToProceed,
      price: state.proceedOrder.price,
      distance: state.proceedOrder.distance
    };
  }
};

const mapDispatchToProps = dispatch => ({
  unreadyToProceed: () => dispatch(unreadyToProceed()),
  startAddOrder: () => dispatch(startAddOrder())
});

AddOrderForm = reduxForm({
  form: "orderForm"
})(AddOrderForm);

AddOrderForm = connect(
  null,
  mapDispatchToProps
)(AddOrderForm);

export default AddOrderForm;
