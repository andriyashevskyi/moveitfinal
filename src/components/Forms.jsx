import React,{Component} from 'react';
import { reduxForm, Field } from 'redux-form'

import Radio from "@material-ui/core/Radio";
import {
  Checkbox,

  SelectField,
  TextField,
  Toggle,
  DatePicker
} from 'redux-form-material-ui'
import {RadioGroup} from '@material-ui/core';

class Form extends Component {
  render() {
    return (
      <form>
    
        <Field name="bestFramework" component={RadioGroup}>
          <Radio value="react" label="React"/>
          <Radio value="angular" label="Angular"/>
          <Radio value="ember" label="Ember"/>
        </Field>
      </form>
    )
  }
}

// Decorate with redux-form
Form = reduxForm({
  form: 'myForm'
})(Form)

export default Form