import React, {Component} from 'react';
import {history} from "../routers/AppRouter";

class ButtonBlock extends Component {
  render() {
    return (
        <div className='start-page-btn-block'>
          <button onClick={history.push('/addorder')} className="styled-btn">Add Order</button>
          <button onClick={history.push('/orders')} className="styled-btn">Orders</button>
          <button onClick={history.push('/settings')} className="styled-btn">Settings</button>
        </div>
    )
  }
}

export default ButtonBlock;