import React from 'react';
import { connect } from 'react-redux';
import OrdersBlockItem from "../views/OrdersBlockItem";
import Grid from "@material-ui/core/es/Grid/Grid";
// @flow

type Props = {
  orders: Array<Object>,
  className: string
}

function OrdersBlock({orders, className}: Props) {
  const children = orders.map((order) => {
    return <Grid key={order.id} item lg={5} md={5} sm={6}>
      <OrdersBlockItem {...order} key={order.id} className="with-border"/></Grid>
  });
  return (
      <div style={{padding:'4px'}}>
      <Grid container spacing={24} justify={'center'}>

        {children}
      </Grid>
      </div>
  )
}


const mapStateToProps = (state) => ( {
  orders: state.orders
} );


export default connect(mapStateToProps)(OrdersBlock);