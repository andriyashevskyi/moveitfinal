//@flow

import React from "react";
import { connect } from "react-redux";
import { Field, reduxForm } from "redux-form";
import {
    required,
    phoneNumber,
    email,
    renderField,
    lastName,
    firstName
} from "../formValidators";
import { startSetUserSettings } from "../actions/userSettings";
import StyledInput from "../views/StyledInput";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
// import Checkbox from "@material-ui/core/Checkbox";

import {
    AutoComplete,
    Checkbox,
    DatePicker,
    TimePicker,
    RadioButtonGroup,
    SelectField,
    Slider,
    TextField,
    Toggle
} from "redux-form-material-ui";

let UserDataSettingsForm = props => {
    const {handleSubmit, initialValues, invalid} = props;
    return (
        <form
            onSubmit={e => {
                e.preventDefault();
                handleSubmit();
            }}
        >
            <Paper style={{padding: "16px"}}>
                <Typography align="center" color="primary" variant="title">
                    Settings
                </Typography>

                <Field
                    name="firstName"
                    component={TextField}
                    validate={[ required, firstName ]}
                    type="text"
                    label={"Имя"}
                    required
                    placeholder="Введите свое имя"
                    fullWidth
                />

                <Field
                    name="lastName"
                    component={TextField}
                    validate={[ required, lastName ]}
                    type="text"
                    placeholder="Введите свою фамилию"
                    label={"Фамилия"}
                    required
                    fullWidth
                />

                <Field
                    name="phone"
                    type="tel"
                    component={TextField}
                    label="Phone number"
                    placeholder="введите номер мобильного"
                    validate={[ required, phoneNumber ]}
                    required
                    fullWidth
                />

                <Field
                    name="email"
                    component={TextField}
                    type="text"
                    placeholder="Email"
                    label="Email"
                    validate={[ required, email ]}
                    required
                    fullWidth
                />

                <label htmlFor="useData">Use account data for order</label>
                <Field name="useData" id="useData" component={Checkbox}/>

                <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    disabled={invalid}
                >
                    Submit
                </Button>
            </Paper>
        </form>
    );
};

const mapStateToProps = state => {
    if (Object.keys(state.userSettings).length === 0) {
        return {initialValues: {...state.user}};
    } else {
        return {initialValues: {...state.userSettings}};
    }
};
const mapDispatchToProps = dispatch => ({
    handleSubmit: () => dispatch(startSetUserSettings())
});

UserDataSettingsForm = reduxForm({
    form: "userDataSettingsForm"
})(UserDataSettingsForm);

UserDataSettingsForm = connect(
    mapStateToProps,
    mapDispatchToProps
)(UserDataSettingsForm);

export default UserDataSettingsForm;
