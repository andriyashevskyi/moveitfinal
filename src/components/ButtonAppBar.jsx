import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SimpleMenu from "../views/SimpleMenu";
import { connect } from "react-redux";
import { bindActionCreators} from 'redux'
import { startUserLogin, startUserLogout } from "../actions/user";
const styles = {
  root: {
    flexGrow: 1,
  },
  flex: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

function ButtonAppBar(props) {
  const {classes,startUserLogin, startUserLogout} = props;
  return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            {/* <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
              <MenuIcon/>
            </IconButton> */}
            <Typography variant="title" color="inherit" className={classes.flex} onClick={() => {
              alert('Main')
            }}>
              MoveIt
            </Typography>
            {props.isAuth
                ? <SimpleMenu photoURL={props.photoURL} logout={startUserLogout}/>
                : <Button 
                  color="inherit" 
                  onClick={startUserLogin}
                  >
                    Login
                  </Button>}


          </Toolbar>
        </AppBar>
      </div>
  );
}

ButtonAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ( {
  isAuth: !!state.user.uid,
  photoURL: state.user.photoURL,
} );
const mapDispatchToProps=(dispatch)=>(
  bindActionCreators({startUserLogin,startUserLogout},dispatch)
)
export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles)(ButtonAppBar));
