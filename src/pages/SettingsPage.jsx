import React, { Component } from "react";
import UserDataSettingsForm from "../components/UserDataSettingsForm";

class SettingsPage extends Component {
    render() {
        return (
            <div style={{width: '75%', margin: 'auto', padding: '20px'}}>
                <UserDataSettingsForm/>
            </div>
        )
    }
}

export default SettingsPage;
