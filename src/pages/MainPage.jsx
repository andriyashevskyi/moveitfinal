import React, { Component } from 'react';
import { connect } from 'react-redux';
import { startUserLogin, startUserLogout } from "../actions/user";
import { getUserOrders, startAddOrder } from "../actions/orders";
import { getUserSettings, startSetUserSettings } from '../actions/userSettings';
import { history } from "../routers/AppRouter";
import AppBar from "@material-ui/core/AppBar";
import ButtonAppBar from "../components/ButtonAppBar";

class MainPage extends Component {
  render() {
    const {userLogin, userLogout, isAuth} = this.props;
    return (
        <div className="main-page">
          {/*<ButtonAppBar/>*/}
          <h1>Стилей на данном этапе нет</h1>
          <div className={'start-page-btn-block'}>
            {isAuth
                ? ( <div>
                  <button onClick={userLogout} className="styled-btn">User logout</button>
                  <button onClick={() => {
                    history.push('/addorder')
                  }} className="styled-btn">Add order
                  </button>
                  <button onClick={() => {
                    history.push('/orders')
                  }} className="styled-btn">Orders
                  </button>
                  <button onClick={() => {
                    history.push('/settings')
                  }} className="styled-btn">Settings
                  </button>
                </div> )
                : <button onClick={userLogin} className="styled-btn">User login</button>}

          </div>

        </div> )
  }
}

const mapStateToProps = (state) => ( {
  isAuth: !!state.user.uid
} );

const mapDispatchToProps = (dispatch) => ( {
  userLogin: () => dispatch(startUserLogin()),
  userLogout: () => dispatch(startUserLogout()),
  addOrder: () => dispatch(startAddOrder()),
  getOrders: () => dispatch(getUserOrders()),
  startSetUserSettings: () => dispatch(startSetUserSettings()),
  getUserSettings: () => dispatch(getUserSettings())
} );

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);