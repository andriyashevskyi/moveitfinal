import React, {Component} from 'react';

import {NavLinks} from "../routers/AppRouter";
import AddOrderForm from "../components/AddOrderForm";

class AddOrderPage extends Component {
  render() {
    return (
        <React.Fragment>
            <AddOrderForm/>         
        </React.Fragment>
    )
  }
}

export default AddOrderPage;