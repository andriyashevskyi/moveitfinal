import React, {Component} from 'react';
import {connect} from 'react-redux';
import {NavLinks} from "../routers/AppRouter";
import OrdersBlock from "../components/OrdersBlock"

class OrdersPage extends Component {
  render() {
    const {ordersExist} = this.props;
    let exist = false;
    if (ordersExist !== 0) {
      exist = true;
    }
    return (
        <React.Fragment>
          {/* <NavLinks/> */}
          <div className="container">
            {exist ? (<OrdersBlock className="grid-2"/>) : (<div>Here no orders</div>)}
          </div>
        </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
  ordersExist: state.orders.length
});
export default connect(mapStateToProps)(OrdersPage);