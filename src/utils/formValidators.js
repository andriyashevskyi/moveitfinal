//@flow
export const required = (value: any) =>
    value || typeof value === "number" ? undefined : "Required";
export const phoneNumber = (value: string) =>
    value && !/^(0|[1-9][0-9]{9})$/.test(value)
    ? "Invalid phone number, must be 10 digits"
    : undefined;

export const email = (value: string) =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? "Invalid email address"
    : undefined;
export const firstName = (value: string) =>
    value && !/^[A-Za-zа-яА-Я--]{3,}$/.test(value)
    ? "Invalid first name"
    : undefined;

export const lastName = (value: string) =>
    value && !/^[A-Za-zа-яА-Я--]{3,}$/.test(value)
    ? "Invalid first name"
    : undefined;