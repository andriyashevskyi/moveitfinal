//@flow
const getDeliveryPrice = (distance: any, pianos: any, liveArea: any, backArea: any): number => {
    const pianosPrice: number = 5000 * parseInt(pianos, 10),
        area: number = parseInt(liveArea, 10) + parseInt(backArea, 10) * 2,
        carsCount: number = Math.ceil(area / 50),
        distanceVal: number = parseInt(distance, 10);

    let finalDistance: number = 0;

    if (distanceVal < 50) {
        finalDistance = 1000 + distanceVal * 10;
    }
    else if (distanceVal >= 50 && distanceVal < 100) {
        finalDistance = 5000 + distanceVal * 8;
    }
    else {
        finalDistance = 10000 + distanceVal * 7;
    }
    
    return pianosPrice + (carsCount * finalDistance);
};

export default getDeliveryPrice;