import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { auth } from "./firebase";
import { database } from "./firebase"
import { userLogin } from "./actions/user";
import { getUserOrders } from "./actions/orders";
import { getUserSettings } from "./actions/userSettings";
import configureStore from './store'
import AppRouter from "./routers/AppRouter";


export const store = configureStore();

const jsx = ( <React.Fragment>
  <Provider store={store}>
    <AppRouter/>
  </Provider>
</React.Fragment> );
let isAppRendered = false;
const renderApp = () => {
  if ( !isAppRendered ) {
    render(jsx, document.getElementById('root'));
    isAppRendered = true;
  }
};


auth.onAuthStateChanged((user) => {
  if ( user ) {
    const displayName = user.displayName;
    const userName = {
      firstName: displayName.split(' ')[ 0 ],
      lastName: displayName.split(' ')[ 1 ]
    };

    store.dispatch(userLogin({...user, ...userName}));
    store.dispatch(getUserOrders());
    store.dispatch(getUserSettings()).then(() => {
      renderApp();
    })

  }
  else {
    renderApp();
  }
});

