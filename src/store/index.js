import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import { reducer as formReducer } from 'redux-form';
import userReducer from "../reducers/user";
import ordersReducer from "../reducers/orders";
import onlineStatusReducer from "../reducers/onlineStatus";
import userSettingsReducer from "../reducers/userSettings";
import orderProceedReducer from "../reducers/orderProceedReducer";
import rootSaga from "../sagas/rootSaga";


const sagaMiddleware = createSagaMiddleware();

const configureStore = () => {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const middleware = [ thunk, sagaMiddleware ];
  const store = createStore(combineReducers({
    user: userReducer,
    orders: ordersReducer,
    online: onlineStatusReducer,
    form: formReducer,
    userSettings: userSettingsReducer,
    proceedOrder: orderProceedReducer
  }), composeEnhancers(applyMiddleware(...middleware)));
  sagaMiddleware.run(rootSaga);
  return store;
};


export default configureStore;