import {
  auth,
  googleAuthProvider
} from "../firebase";


export const userLogin = ({
  uid,
  phone = '',
  email = '',
  firstName = '',
  lastName = '',
  photoURL = ''
} = {}) => ({
  type: 'USER_LOGIN',
  user: {
    uid,
    email,
    phone,
    firstName,
    lastName,
    photoURL
  }
});


export const startUserLogin = () => {
  return (dispatch) => {
    return auth.signInWithPopup(googleAuthProvider)
      .then((user) => {
        console.log('--->', user);
        const displayName = user.user.displayName;
        /*   const userName = {
             firstName: displayName.split(' ')[0],
             lastName: displayName.split(' ')[1]
           };

           dispatch(userLogin({...user.user, ...userName}))*/
      })
      .catch((error) => {
        if (error.code === 'auth/popup-closed-by-user') {
          dispatch(userLogout());
        }
      })
  }
};


/**
 * action creator for user logout
 * @returns {{type: string}}
 */
const userLogout = () => ({
  type: 'USER_LOGOUT'
});

/**
 * start user logout function
 * @returns {Function}
 */
export const startUserLogout = () => {
  return (dispatch) => {
    auth.signOut()
      .then(() => (dispatch(userLogout())));
  }
};