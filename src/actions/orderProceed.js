export const readyToProceed = () => ({
  type: 'ORDER_READY_TO_PROCEED'
});

export const unreadyToProceed = () => ({
  type: 'ORDER_UNREADY_TO_PROCEED'
});

export const setAddressToProceed = ({
    originalAddress = '',
    destinationAddress = '',
  } = {}) =>
  ({
    type: 'SET_ADDRESS_TO_PROCEED',
    data: {
      originalAddress,
      destinationAddress,
    }
  });

export const setPriceToProceed = ({
    price = 1,
    distance = 1,
    pianoCount = 1,
  } = {}) =>
  ({
    type: 'SET_PRICE_TO_PROCEED',
    data: {
      price,
      distance,
      pianoCount
    }
  });