export const onlineStatusOn = () => ({
  type: 'SITE_ONLINE'
});

export const onlineStatusOff = () => ({
  type: 'SITE_OFFLINE'
});