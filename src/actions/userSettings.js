import { database } from "../firebase";

export const setUserSettings = (data) => ({
    type: 'SET_USER_SETTINGS',
    data
});


export const startSetUserSettings = () => {
    return (dispatch, getState) => {
        const {uid} = getState().user;
        console.log(getState().form);
        const {firstName, lastName, email, phone, useData = false} = getState().form.userDataSettingsForm.values;
        const data = {firstName, lastName, email, phone, useData};
        return database.ref(`users/${uid}/settings`).set(data)
            .then(() => {
                dispatch(setUserSettings(data));
            })
    }
};
export const getUserSettings = () => {
    return (dispatch, getState) => {
        const {uid} = getState().user;
        console.log('getting data', uid);
        return database.ref(`users/${uid}/settings`).once('value').then((dataSnapshot) => {
            dispatch(setUserSettings(dataSnapshot.val()));
        })
    }
};