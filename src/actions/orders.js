import { database } from "../firebase";

export const addOrder = (order) => ( {
  type: 'ADD_ORDER',
  order
} );


export const startAddOrder = () => ( {
  type: 'START_ADD_ORDER'
} );


export const setOrders = (orders) => ( {
  type: 'SET_ORDERS',
  orders

} );

export const getUserOrders = () => ( {
  type: 'GET_ORDERS'
} );