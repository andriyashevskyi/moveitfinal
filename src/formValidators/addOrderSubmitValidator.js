//@flow
import { SubmissionError } from 'redux-form';
import { store } from "../index";
import { setPriceToProceed, setAddressToProceed, readyToProceed } from "../actions/orderProceed";
import getDeliveryPrice from "../priceCalculation";

const apiKey: string = 'AIzaSyDE1utw7PaaXRAx4-S00of42hjBIqSGNdA';
const url: string = 'https://maps.googleapis.com/maps/api/geocode/json?address';
const distanceMatrixUrl = 'https://maps.googleapis.com/maps/api/distancematrix/json?';
const directionsApi: string = 'https://maps.googleapis.com/maps/api/directions/json?';

// вбрасываем ошибку в форму
function throwError(errors) {
    throw new SubmissionError({...errors})
}

// запрос к гугл api
/**
 *
 * @param address
 * @returns {Promise<any>}
 */
async function fetchFromGoogleMaps(address: string) {
    const fullUrl = `${url}=${address}&key=${apiKey}`;
    const response = await fetch(fullUrl);
    const body = await response.json();
    if (response.ok) {
        return body
    }
    else {
        return body
    }


}

// получаем дистанцию
/**
 *
 * @param original
 * @param destination
 * @returns {Promise<any>}
 */
async function fetchDistanceFromGoogleMaps(original: string, destination: string) {
    const fullUrl = `${directionsApi}origin=place_id:${original}&destination=place_id:${destination}&mode=driving&language=en&key=${apiKey}`;
    const response = await fetch(fullUrl,

        /*{
          'method': 'GET',
          'headers': {
              'Content-Type': 'application/json',
              'Access-Control-Allow-Origin': '*'
          },
        }*/);
    const body = await response.json();
    if (response.ok) {
        return body
    }
    else {
        return body
    }


}

// промис для подсчета дистанции
const distancePromise = (original: string, destination: string) => (fetchDistanceFromGoogleMaps(original, destination));

// получаем place_id и информацию о аддрессе
export async function getAddressesId(address1: string, address2: string) {
    const [ origin, dest ] = await Promise.all([
        fetchFromGoogleMaps(address1),
        fetchFromGoogleMaps(address2)
    ]);
}


const checkAddresses = (address1: string, address2: string) => Promise.all([ fetchFromGoogleMaps(address1),
    fetchFromGoogleMaps(address2) ]);


//фнукция обработчик формы
//TODO просмотреть в каком формате получаем данные из хранилища
export function submit(values: { originalAddress: string, destinationAddress: string, bigThings: string, liveArea: string, backArea: string }) {
    return checkAddresses(values.originalAddress, values.destinationAddress)
        .then((data) => {
            const [ origin, dest ] = data;
            console.log(origin);
            console.log(dest);

            // проверки будут переписаны

            if (origin.status === 'ZERO_RESULTS' && dest.status === 'ZERO_RESULTS') {
                throwError({
                    originalAddress: 'Мы не нашли адрес',
                    destinationAddress: 'Мы не нашли адрес'
                });

            }
            else if (origin.status === 'ZERO_RESULTS') {
                throwError({
                    originalAddress: 'Мы не нашли адрес',
                });
            } else if (dest.status === 'ZERO_RESULTS') {
                throwError({
                    destinationAddress: 'Мы не нашли адрес'
                })
            }
            if (origin.results.length > 1 && dest.results.length > 1) {
                throwError({
                    originalAddress: 'пожалуйста, введите более конкретные данные сообщение 1',
                    destinationAddress: 'пожалуйста, введите более конкретные данные сообщение 2'
                });

            }
            else if (origin.results.length > 1) {
                throwError({
                    originalAddress: 'пожалуйста, введите более конкретные данные сообщение 1',
                });
            } else if (dest.results.length > 1) {
                throwError({
                    destinationAddress: 'пожалуйста, введите более конкретные данные сообщение '
                })
            }

            if (origin.results[ 0 ].hasOwnProperty('partial_match') && dest.results[ 0 ].hasOwnProperty('partial_match')) {
                throwError({
                    originalAddress: 'отработали partial_match',
                    destinationAddress: 'отработали partial_match'
                })
            }
            else if (origin.results[ 0 ].hasOwnProperty('partial_match')) {
                throwError({
                    originalAddress: 'отработали partial_match',
                })
            } else if (dest.results[ 0 ].hasOwnProperty('partial_match')) {
                throwError({
                    destinationAddress: 'отработали partial_match',
                })
            }


            const dataToReturn = {
                originId: origin.results[ 0 ][ 'place_id' ],
                originalAddress: origin.results[ 0 ][ 'formatted_address' ],
                destId: dest.results[ 0 ][ 'place_id' ],
                destinationAddress: dest.results[ 0 ][ 'formatted_address' ]
            };


            return dataToReturn;

        })
        .then((data) => {
            store.dispatch(setAddressToProceed(data));
            distancePromise(data.originId, data.destId).then((data) => {
                //TODO сделать проверку на статус ответа Zero Results
                console.log(data);
                let distance = data.routes[ "0" ].legs[ "0" ].distance.value;
                console.log(distance);
                let finalDistance: number = Math.ceil(distance / 1000);
                const pianoCount = values.bigThings.split(',').filter(item => item.trim() === 'piano').length;
                const backArea = parseInt(values.backArea, 10);
                const liveArea = parseInt(values.liveArea, 10);
                const price: number = getDeliveryPrice(finalDistance, pianoCount, liveArea, backArea);
                console.log(price);
                const dataToProceed = {
                    price: price,
                    distance: finalDistance,
                    pianoCount: pianoCount
                };
                store.dispatch(setPriceToProceed(dataToProceed));
                store.dispatch(readyToProceed());

            });

        })

}


