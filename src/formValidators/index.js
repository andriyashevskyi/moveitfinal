import React from "react";
import { Field } from "redux-form";

export const required = value =>
  value || typeof value === "number" ? undefined : "Required";
export const phoneNumber = value =>
  value && !/^(0|[1-9][0-9]{9})$/.test(value)
    ? "Invalid phone number, must be 10 digits"
    : undefined;

export const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? "Invalid email address"
    : undefined;
export const firstName = value =>
  value && !/^[A-Za-zа-яА-Я--]{3,}$/.test(value)
    ? "Invalid first name"
    : undefined;

export const lastName = value =>
  value && !/^[A-Za-zа-яА-Я--]{3,}$/.test(value)
    ? "Invalid first name"
    : undefined;

export const address = value =>
  value && !/^[a-zA-Zа-яА-Я0-9-/,\s]{6,}$/.test(value)
    ? "Invalid address"
    : undefined;

export const bigThings = value =>
  value && !/([a-zA-Zа-яА-Я-]+)(,\s*[a-zA-Zа-яА-Я-]+)*/.test(value)
    ? "Please separate it with coma"
    : undefined;

export const area = value =>
  value && !/^([0-9]{1,})(.|,)?([0-9]{1,})?/.test(value)
    ? "get number or 0"
    : undefined;

export const renderField = ({
  input,
  label,
  type,
  disabled,
  placeholder,
  meta: { touched, error, warning }
}) => (
  <div>
    <label>{label}</label>
    <div>
      <input
        {...input}
        placeholder={placeholder}
        type={type}
        disabled={disabled}
      />
      {touched &&
        ((error && <span>{error}</span>) ||
          (warning && <span>{warning}</span>))}
    </div>
  </div>
);

export const FirstName = props => {
  return (
    <Field
      name="firstName"
      type="text"
      placeholder="First Name"
      component={renderField}
      validate={[required, firstName]}
      {...props}
    />
  );
};
export const LastName = props => {
  return (
    <Field
      name="lastName"
      type="text"
      placeholder="Last Name"
      component={renderField}
      validate={[required, lastName]}
      {...props}
    />
  );
};

export const Phone = props => {
  return (
    <Field
      name="phone"
      type="tel"
      placeholder="Phone"
      component={renderField}
      validate={[required, phoneNumber]}
      {...props}
    />
  );
};

export const Email = props => {
  return (
    <Field
      name="email"
      type="tel"
      placeholder="Email"
      component={renderField}
      validate={[required, email]}
      {...props}
    />
  );
};

export const OriginalAddress = props => {
  return (
    <Field
      name="originalAddress"
      type="text"
      placeholder="Hello"
      component={renderField}
      validate={[required, address]}
      {...props}
    />
  );
};

export const DestinationAddress = props => {
  return (
    <Field
      name="destinationAddress"
      type="text"
      placeholder="From"
      component={renderField}
      validate={[required, address]}
      {...props}
    />
  );
};

export const LiveArea = props => {
  return (
    <Field
      name="liveArea"
      type="number"
      placeholder="1.0"
      component={renderField}
      validate={[required, area]}
      {...props}
    />
  );
};

export const BackArea = props => {
  return (
    <Field
      name="backArea"
      type="number"
      placeholder="0"
      component={renderField}
      validate={[required, area]}
      {...props}
    />
  );
};

export const BigThings = props => {
  return (
    <Field
      name="bigThings"
      type="text"
      placeholder="piano, tv"
      component={renderField}
      validate={[required, bigThings]}
      {...props}
    />
  );
};
