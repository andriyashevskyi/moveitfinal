import { takeEvery } from 'redux-saga/effects';
import { getOrdersWatcher, startAddOrderWatcher } from "./ordersSaga";


function* rootSaga() {
  yield takeEvery('GET_ORDERS', getOrdersWatcher);
  yield takeEvery('START_ADD_ORDER', startAddOrderWatcher);
}

export default rootSaga;