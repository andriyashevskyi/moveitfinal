export const getUserUid = (state) => state.user.uid;

export const getFormValues = (state) => state.form.orderForm.values;

export const getProceedOrder = (state) => state.proceedOrder;