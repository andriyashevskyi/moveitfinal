import { database } from "../firebase";
import { put, select } from 'redux-saga/effects'
import { addOrder, setOrders } from "../actions/orders";
import { getFormValues, getProceedOrder, getUserUid } from "./sagasSelect";

// в идеале передавать в actionCreator  id пользователя,
export function* getOrdersWatcher() {
  try {
    const uid = yield select(getUserUid);
    const ordersArray = [];
    const snapshot = yield database.ref(`users/${uid}/orders/`).once('value');
    yield console.log(snapshot.val());
    snapshot.forEach((childSnapshot) => {
      ordersArray.push({id: childSnapshot.key, ...childSnapshot.val()})
    });
    yield put(setOrders(ordersArray));

  }
  catch ( err ) {
//TODO проверка на получение данных в случае неверного запроса, firebase возвращает null
  }
}

// TODO передавать данные формы в качестве аргументов функции
export function* startAddOrderWatcher() {
  try {
    console.log('start add Order Watcher');
    const uid = yield select(getUserUid);
    const orderForm = yield select(getFormValues);
    const proceedOrder = yield select(getProceedOrder);
    const {
            originalAddress,
            destinationAddress,
            price,
            distance,
            pianoCount,
          } = proceedOrder;
    const finalOrder = {
      originalAddress,
      destinationAddress,
      price,
      distance,
      pianoCount
    };
    const ref = yield database.ref(`users/${uid}/orders`).push({...orderForm, ...finalOrder});
  debugger;
    console.log(ref);
    yield put(addOrder({
          id: ref.id,
          ...orderForm,
          ...finalOrder
        }
    ))

  }
  catch ( err ) {

  }

}