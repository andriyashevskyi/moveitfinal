import React from 'react';
import {connect} from 'react-redux';
import {Route, Redirect} from 'react-router-dom';

const PrivateRote = (props) => {
  const {isAuth, ...rest} = props;
  return (
      isAuth
          ? <Route {...rest}/>
          : <Redirect to='/'/>
  )
};

const mapStateToProps = (state) => ({
  isAuth: !!state.user.uid
});

export default connect(mapStateToProps)(PrivateRote);