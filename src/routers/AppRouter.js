import React from 'react';
import { Router, Route, Switch, Link } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory'
import CssBaseline from '@material-ui/core/CssBaseline';
import PrivateRoute from './PrivateRoute';
import MainPage from "../pages/MainPage";
import SettingsPage from "../pages/SettingsPage";
import AddOrderPage from "../pages/AddOrderPage";
import OrdersPage from "../pages/OrdersPage";
import OrderPage from "../pages/OrderPage";
import ButtonAppBar from "../components/ButtonAppBar";
import List from "@material-ui/core/es/List/List";
import ListItem from "@material-ui/core/es/ListItem/ListItem";

export const history = createBrowserHistory();
export const NavLinks = () => (
    <List component={'nav'}>
      <ListItem>
        <Link to='/'>Home</Link>
      </ListItem>
      <ListItem>
        <Link to='/settings'>Settings</Link>
      </ListItem>
      <ListItem>
        <Link to='/addorder'>Add order</Link>
      </ListItem>
      <ListItem>
        <Link to='/orders'>Orders</Link>
      </ListItem>
    </List>
);
const AppRouter = () => (
    <Router history={history}>
      <React.Fragment>
        <CssBaseline/>
        <ButtonAppBar/>
        <Switch>
          <Route exact path='/' render={() => <MainPage/>}/>
          <PrivateRoute path='/settings' component={SettingsPage}/>
          <PrivateRoute path='/addorder' component={AddOrderPage}/>
          <PrivateRoute path='/orders' component={OrdersPage}/>
          <PrivateRoute path='/order/' component={OrderPage}/>
          <Route render={() => <div>404page</div>}/>
        </Switch>
      </React.Fragment>
    </Router>
);

export default AppRouter;
