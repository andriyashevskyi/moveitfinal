const getDeliveryPrice = (distance, pianos, liveArea, backArea) => {
  const pianosPrice = 5000 * parseInt(pianos, 10),
      area = parseInt(liveArea, 10) + parseInt(backArea, 10) * 2,
      carsCount = Math.ceil(area / 50),
      distanceVal = parseInt(distance, 10);

  console.log('-----> pianosprice', pianosPrice);
  console.log('-----> area', area);
  console.log('-----> carscount', carsCount);
  console.log('-----> distance', distanceVal);

  let finalDistance = 0;

  if (distanceVal < 50) {
    finalDistance = 1000 + distanceVal * 10;
  }
  else if (distanceVal >= 50 && distanceVal < 100) {
    finalDistance = 5000 + distanceVal * 8;
  }
  else {
    finalDistance = 10000 + distanceVal * 7;
  }
  console.log('-----> distance price', finalDistance);
  return pianosPrice + (carsCount * finalDistance);
};

export default getDeliveryPrice;