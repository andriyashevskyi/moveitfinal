import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyAIbGtMJsbqCHBmS_w8B8t6WrcmqrS5VKQ",
  authDomain: "moveitproject-a5bf5.firebaseapp.com",
  databaseURL: "https://moveitproject-a5bf5.firebaseio.com",
  projectId: "moveitproject-a5bf5",
  storageBucket: "moveitproject-a5bf5.appspot.com",
  messagingSenderId: "20945265377"
};

firebase.initializeApp(config);

export const database = firebase.database();
firebase.auth().languageCode='en';
export const auth = firebase.auth();

export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();