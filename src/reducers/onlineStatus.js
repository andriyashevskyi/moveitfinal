const onlineStatusReducer = (state = {firstLoad: true}, action) => {
  switch (action.type) {
    case 'SITE_ONLINE':
      return {...state, online: true};

    case 'SITE_OFFLINE':
      return {firstLoad: false, online: false};
    default:
      return state;
  }
};

export default onlineStatusReducer;