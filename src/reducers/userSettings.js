const userSettingsReducer = (state = {}, action) => {
  switch (action.type) {
    case 'SET_USER_SETTINGS':
      return {...action.data};
    default:
      return state;
  }
};

export default userSettingsReducer;