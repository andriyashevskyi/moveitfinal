const ordersReducer = (state = [], action) => {
  switch (action.type) {
    case 'ADD_ORDER':
      return [
        ...state,
        ( action.order )
      ];
    case 'START_ADD_ORDER':
      return state;
    case 'SET_ORDERS':
      return action.orders;
    case 'GET_ORDERS':
      return state;
    default:
      return state;
  }
};


export default ordersReducer;