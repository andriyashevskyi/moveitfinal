const orderProceedReducer = (state = {orderReadyToProceed: false}, action) => {
  switch (action.type) {
    case 'ORDER_READY_TO_PROCEED':
      return {
        ...state,
        ...{orderReadyToProceed: true}
      };
    case 'ORDER_UNREADY_TO_PROCEED':
      return {
        ...state,
        ...{orderReadyToProceed: false}
      };
    case 'SET_ADDRESS_TO_PROCEED':
      return {
          ...state,
        ...(action.data)
      };
    case 'SET_PRICE_TO_PROCEED':
      return {
        ...state,
        ...(action.data)
      };
    default: {
      return state;
    }
  }

};

export default orderProceedReducer;

