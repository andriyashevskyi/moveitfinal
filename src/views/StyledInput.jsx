// @flow
import React from "react";
import TextField from "@material-ui/core/TextField";
import { Checkbox } from "@material-ui/core";


type Props = {
    type: string,
    meta: { error: string, valid: boolean, touched: boolean, warning: mixed },
    input: { value: string, onChange: void },
    required?: boolean,
    label?: string,
    placeholder?: number | string,
    name: string
};

function StyledInput(props: Props) {
    const {
        input: {value, onChange},
        meta: {touched, error, valid, warning},
        placeholder,
        required,
        label,
        type,
        name
    } = props;

    //console.log(touched);
    let error1 = touched && error ? Boolean(error) : null;
    console.log("error ---->", error1);

    return type !== "checkbox" ? (
        <TextField
            name={name}
            value={value}
            onChange={onChange}
            error={touched && (error || warning) ? Boolean(error || warning) : false}
            placeholder={placeholder}
            required={required}
            label={label}
            type={type}
            helperText={!valid && !touched ? error : null}
            fullWidth
            onBlur={() => {
                console.log("touched this fucking input");
            }}
            margin="dense"
        />
    ) : (
               <Checkbox value={value} onChange={onChange}/>
           );
}

export default StyledInput;
