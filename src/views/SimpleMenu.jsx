//@flow
import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { Link } from "react-router-dom";

import ImageAvatar from './ImageAvatar';

type SimpleMenuProps = {
    photoURL?: string,
    logout?: void
}

class SimpleMenu extends Component<SimpleMenuProps> {
    state = {
        anchorEl: null,
    };

    handleClick = event => {
        this.setState({anchorEl: event.currentTarget});
    };

    handleClose = () => {
        this.setState({anchorEl: null});
    };

    render() {
        const {anchorEl} = this.state;
        const {photoURL, logout} = this.props;

        return (
            <div>
                <Button
                    aria-owns={anchorEl ? 'simple-menu' : null}
                    aria-haspopup="true"
                    onClick={this.handleClick}
                >
                    <ImageAvatar photoURL={photoURL}/>
                </Button>
                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={this.handleClose}
                >
                    <MenuItem onClick={this.handleClose}><Link to='/addorder'>Add Order</Link></MenuItem>
                    <MenuItem onClick={this.handleClose}><Link to='/orders'>My orders</Link></MenuItem>
                    <MenuItem onClick={this.handleClose}><Link to='/settings'>Settings</Link></MenuItem>
                    <MenuItem onClick={logout}>Logout</MenuItem>
                </Menu>
            </div>
        );
    }
}

const a: number = '5';


export default SimpleMenu;
