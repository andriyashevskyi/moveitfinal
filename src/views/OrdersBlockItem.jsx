import React from 'react';
import PropTypes from 'prop-types';
import {history} from "../routers/AppRouter";

function OrdersBlockItem({id, distance, originalAddress, destinationAddress, price, className}) {
  return (
      <div className={className}>
        <h2>Order id:</h2>
        <p>{id}</p>
        <h3>From</h3>
        <p>{originalAddress}</p>
        <h3>To</h3>
        <p>{destinationAddress}</p>
        <h3>Price</h3>
        <p>{price}</p>
        <h3>Distance</h3>
        <p>{distance}</p>
        <button className="styled-btn" onClick={() => (history.push(`/order/${id}`))}>Read more</button>
      </div>
  )
}

OrdersBlockItem.propTypes = {
  id: PropTypes.string.isRequired,
  originalAddress: PropTypes.string.isRequired,
  destinationAddress: PropTypes.string.isRequired,
  distance: PropTypes.number.isRequired,
  price: PropTypes.number.isRequired,
  className: PropTypes.string,

};
OrdersBlockItem.defaultProps = {
  id: 'order id',
  originalAddress: "Street 12 NY,llo",
  destinationAddress: "Street 14 OK,OOLo",
  distance: "12",
  price: "1600",

};

export default OrdersBlockItem;