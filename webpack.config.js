const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
  mode: 'development',
  entry:
      ["babel-polyfill", "./src/index.js"]
  ,
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist',
    // overlay:true,
    historyApiFallback: true
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new HtmlWebpackPlugin({
      inject: false,
      template: 'node_modules/html-webpack-template/index.ejs',
      appMountId: 'root',
      title: 'Production',
      mobile: true

    })
  ],
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/'
  },
  module: {
    rules: [{
      test: /\.s?css$/,
      use: [{loader: "style-loader"}, {loader: "css-loader"}, {loader: "sass-loader"}]
    },
      {
        test: /\.jsx?$/,
        resolve: {
          extensions: ['.js', '.jsx'],
        },
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['react'],
        }
      },

      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {}
          }
        ]
      }


    ]
  }
};