const webpack = require('webpack');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const common = require('./webpack.config.js');

module.exports = merge(common, {
      mode: 'production',
       devtool: 'source-map',
    plugins: [
       new UglifyJSPlugin({
             sourceMap: true
       }),
           new webpack.DefinePlugin({
                 'process.env.NODE_ENV': JSON.stringify('production')
     })
]
});